# moz_bookmarks.tcl

create bookmarks.html from firefox places.sqlite file

this script emulates *Import and Backup->Export Bookmarks to HTML...*

## requirements

-   [tcl](http://www.tcl-lang.org/)
-   [sqlite3 tcl interface](https://sqlite.org/tclsqlite.html)

## usage

```
$ tclsh moz_bookmarks.tcl /where/to/places.sqlite > bookmarks.html
```

generated bookmarks.html can be imported to other profile of firefox
by *Import Bookmarks from HTML...*.

notes: *Bookmarks Toolbar* and *Other Bookmarks* folders are not reproduced as originally.

## issue

- emoji are not supported (solved in tcl version 8.6.11?)

  [tcl do not support emoji by default](https://wiki.tcl-lang.org/page/emoji).
  tcl version 8.6.11 support emoji partially without TCL_UTF_MAX=6.

- moz_bookmarks.tcl do not export favicons (ICON, ICON_URI tag)

- description tag `<DD>`

  firefox do not export `<DD>` tag anymore, but moz_bookmarks.tcl export it.

- LAST_CHARSET element

  firefox remove LAST_CHARSET element if UTF-8, but moz_bookmarks.tcl don't remove it.
