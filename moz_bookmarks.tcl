#!/usr/bin/tclsh

package require sqlite3

# show usage
set db_filename [lindex $argv 0]
if {$db_filename eq ""} {
	puts "usage: $argv0 <places.sqlite>"
	exit
}

# use utf-8 in output encoding, not a platform-dependent encoding.
# set newline character to lf like firefox does.
fconfigure stdout -encoding utf-8 -translation lf

# open sqlite database
sqlite3 db1 $db_filename -readonly 1

# puts some spaces for indentation
# deep: indentation level
proc indent {deep} {
	puts -nonewline [string repeat {    } $deep]
}

# encode &'"<> in str
proc html_encode {str} {
	return [string map {& &amp; ' &#39; \" &quot; < &lt; > &gt;} $str]
}

# print <DD> description
# id: moz_bookmard.id
# deep: recursion count, used of indentation
proc print_description {id deep} {
	db1 eval "SELECT content FROM moz_items_annos WHERE $id = item_id" desc_values {
		# indent
		indent $deep
		# description
		puts "<DD>[html_encode $desc_values(content)]"
	}
}

# escape doublequotation of url that appears in bookmarklet
proc escape_url_dq {url} {
	return [string map {\" %22} $url]
}

# puts moz_bookmarks.id information recursively
# id: moz_bookmarks.id
# deep: recursion count, used of indentation
proc print_bookmark {id deep} {
	# LEFT JOIN: moz_bookmarks as b, moz_places as h, moz_keywords as k, moz_annos as a
	# condition: b.fk = h.id = k.place_id = a.place_id
	db1 eval "SELECT h.id, b.dateAdded, b.lastModified, b.type, b.title, h.url, k.keyword, k.post_data, a.content AS last_charset, b.guid
	FROM moz_bookmarks AS b
	LEFT JOIN moz_places AS h ON b.fk = h.id
	LEFT JOIN moz_keywords AS k ON h.id = k.place_id
	LEFT JOIN moz_annos AS a ON h.id = a.place_id
	WHERE b.id = $id" query_values {
		#parray query_values

		# ADD_DATE, LAST_MODIFIED; adjust to bookmarks.html
		set add_date [expr $query_values(dateAdded) / 1000000]
		set last_modified [expr $query_values(lastModified) / 1000000]
		# moz_bookmarks.type
		#   1: bookmark, 2: folder, 3: separator
		if {$query_values(type) == 1} {
			# bookmark item
			# indent
			indent $deep
			# puts url, add date, last modified
			set newurl [escape_url_dq $query_values(url)]; # \" => %22
			puts -nonewline "<DT><A HREF=\"$newurl\" ADD_DATE=\"$add_date\" LAST_MODIFIED=\"$last_modified\""

			# puts bookmark keyword
			if {$query_values(keyword) != ""} {
				puts -nonewline " SHORTCUTURL=\"$query_values(keyword)\""
			}
			# puts post_data
			if {$query_values(post_data) != ""} {
				puts -nonewline " POST_DATA=\"[html_encode $query_values(post_data)]\""
			}
			# puts last charset
			if {$query_values(last_charset) != ""} {
				puts -nonewline " LAST_CHARSET=\"$query_values(last_charset)\""
			}
			# puts title
			puts ">[html_encode $query_values(title)]</A>"
			# puts description
			print_description $id $deep
		} elseif {$query_values(type) == 2} {
			# folder item
			if {$query_values(guid) == "menu________"} {
				# Bookmarks Menu special folder: don't print
				incr deep -1
			} else {
				# not a Bookmarks Menu folder
				# indent
				indent $deep
				# puts add date, last modified
				puts -nonewline "<DT><H3 ADD_DATE=\"$add_date\" LAST_MODIFIED=\"$last_modified\""

				if {$query_values(guid) == "toolbar_____"} {
					# Bookmarks Toolbar special folder
					puts -nonewline { PERSONAL_TOOLBAR_FOLDER="true"}
				} elseif {$query_values(guid) == "unfiled_____"} {
					# Other Bookmarks special folder
					puts -nonewline { UNFILED_BOOKMARKS_FOLDER="true"}
				}
				# puts title
				puts ">$query_values(title)</H3>"
				# puts description
				print_description $id $deep
				# indent
				indent $deep
				puts "<DL><p>"
			}

			# search items which parent = id from moz_bookmarks
			db1 eval "SELECT id FROM moz_bookmarks WHERE parent = $id" bmk {
				# recursion
				print_bookmark $bmk(id) [expr $deep + 1]
			}

			# not a Bookmarks Menu folder, puts </DL><p>
			if {$query_values(guid) != "menu________"} {
				# indent
				indent $deep
				puts "</DL><p>"
			}
		} elseif {$query_values(type) == 3} {
			# separator item
			# indent
			indent $deep
			# puts separator
			puts -nonewline "<HR>"
		}

	}
}

# print header
puts "<!DOCTYPE NETSCAPE-Bookmark-file-1>
<!-- This is an automatically generated file.
     It will be read and overwritten.
     DO NOT EDIT! -->
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
<TITLE>Bookmarks</TITLE>
<H1>Bookmarks Menu</H1>

<DL><p>"

# Bookmarks Menu
print_bookmark 2 1
# Bookmarks Toolbar
print_bookmark 3 1
# Other Bookmarks
print_bookmark 5 1

puts {</DL>}

db1 close
